import okaydesigner from 'Designer'
import Bar from './ctrl/Bar.od'
import Text from './ctrl/Text.od'
import Base from './ctrl/Base.od'
import Link from './ctrl/Link.od'
import Button from './ctrl/Button.od'
import Table from './ctrl/Table.od'
import P from './ctrl/P.od'
import Ul from './ctrl/Ul.od'
import Form from './ctrl/Form.od'
import Input from './ctrl/Input.od'
import CheckBox from './ctrl/CheckBox.od'
import Radio from './ctrl/Radio.od'
import Select from './ctrl/Select.od'
import Textarea from './ctrl/Textarea.od'
import Image from './ctrl/Image.od'
import Icon from './ctrl/Icon.od'
import AuiList from './ctrl/AuiList.od'
import Column from './ctrl/Column.od'
import AuiButton from './ctrl/AuiButton.od'
import AuiInput from './ctrl/AuiInput.od'
import AuiLable from './ctrl/AuiLabel.od'
import AuiMediaList from './ctrl/AuiMediaList.od'
import AuiRadio from './ctrl/AuiRadio.od'
import AuiSearch from './ctrl/AuiSearch.od'
import AuiSelect from './ctrl/AuiSelect.od'
import AuiTextarea from './ctrl/AuiTextarea.od'
import Card from './ctrl/Card.od'
import ChatBox from './ctrl/ChatBox.od'
import ChatHeader from  './ctrl/ChatHeader.od'
import ChatItem from './ctrl/ChatItem.od'
import Footer from './ctrl/Footer.od'
import Grid from './ctrl/Grid.od'
import Progress from './ctrl/Progress.od'
import Range from './ctrl/Range.od'
import Switch from './ctrl/Switch.od'
import Tab from './ctrl/Tab.od'
import Tabs from './ctrl/Tabs.od'
import FormUl from './ctrl/FormUl.od'
import Tip from './ctrl/Tip.od'
window.od = {
    auiinput:AuiInput,
    auilabel:AuiLable,
    auimedialist:AuiMediaList,
    auiradio:AuiRadio,
    auisearch:AuiSearch,
    auiselect:AuiSelect,
    auitextarea:AuiTextarea,
    card:Card,
    chatbox:ChatBox,
    chatheader:ChatHeader,
    chatitem:ChatItem,
    footer:Footer,
    grid:Grid,
    progress:Progress,
    range:Range,
    switch:Switch,
    tab:Tab,
    tabs:Tabs,
    formui:FormUl,
    tip:Tip,
    base:Base,
    text:Text,
    bar:Bar,
    link:Link,
    button:Button,
    table:Table,
    p:P,
    ul:Ul,
    form:Form,
    input:Input,
    checkbox:CheckBox,
    radio:Radio,
    select:Select,
    textarea:Textarea,
    img:Image,
    icon:Icon,
    auilist:AuiList,
    column:Column,
    auibutton:AuiButton,

}

